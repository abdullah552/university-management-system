<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(StudentsSeeder::class);
         $this->call(DepartmentsSeeder::class);
         $this->call(SectionSeeder::class);
         $this->call(SemesterSeeder::class);
         $this->call(BooksSeeder::class);
         $this->call(CoursesSeeder::class);
         $this->call(FacultiesSeeder::class);

  
      
    }
}
