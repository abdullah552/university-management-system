<?php

use Faker\Generator as Faker;

$factory->define(App\Semester::class, function (Faker $faker) {
    $rand_num        = $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8]);
    return [
        'semester_no'   => $faker->year,
        'year'          => $rand_num, 
    ];
});
