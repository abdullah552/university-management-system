<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    return [
        'first_name' => $faker->firstNameMale,
        'last_name'  => $faker->lastName,
        'contact'    => $faker->e164PhoneNumber,
        'address'    => $faker->address(50),
        'gender'     => $gender 
    ];

});
