<?php

use Faker\Generator as Faker;

$factory->define(App\Course::class, function (Faker $faker) {
    $course_name = $faker->randomElement(['Electronics', 'Applied Physics', 'DLD', 'MVC', 'SQL', 'Data Structures']);
    return [
        'course_name'  =>  $course_name, 
    ];
});
