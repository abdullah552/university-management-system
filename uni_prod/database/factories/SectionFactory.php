<?php

use Faker\Generator as Faker;

$factory->define(App\Section::class, function (Faker $faker) {

           
    $rand_num        = $faker->randomElement([1, 2, 3, 4, 5, 6, 7]);
    $class_room      = $faker->randomElement(['A-1', 'A-2', 'A-3', 'A-3', 'B-1', 'B-2', 'B-3', 'B-4']);     
    $time_span       = $faker->randomElement(['1:30-2:30', '2:30-3:30', '3:30-4:30', '4:30-5:30', '5:30-6:30']);  
    $section_days    = $faker->randomElement(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']);  
   
    
    return [
        'slots'             => $rand_num,
        'course_id'         => $rand_num,
        'faculty_id'        => $rand_num,
        'semester_id'       => $rand_num,
        'class_room'        => $class_room,
        'time_span'         => $time_span,
        'section_days'      => $section_days
    ];
});
