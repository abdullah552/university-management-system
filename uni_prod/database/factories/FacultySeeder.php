<?php

use Faker\Generator as Faker;

$factory->define(App\Faculty::class, function (Faker $faker) {
    $rand_num        = $faker->randomElement([1, 2, 3, 4, 5, 6, 7]);
  
    return [
        'semester_no'  => $rand_num, 
        'semester_year' => $faker->year
    ];
});
