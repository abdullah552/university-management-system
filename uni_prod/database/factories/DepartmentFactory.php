<?php

use Faker\Generator as Faker;

$factory->define(App\Department::class, function (Faker $faker) {
    $department = $faker->randomElement(['EE', 'CS', 'BBA', 'Chemical', 'Architecture', 'Medical']);
    return [
            'dep_name' => $department,
    ];
});
