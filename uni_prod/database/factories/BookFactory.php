<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    $book_name = $faker->randomElement(['Science', 'Maths', 'Computer', 'English']);
    return [
        
            'book_name'   => $book_name,  
    ];
});
