<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    public function section(){
        return $this->hasMany('App\Section');
    }

    public function students(){
        return $this->belongdToMany('App\Student');
    }
}
