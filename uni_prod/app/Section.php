<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public function semester(){
        return $this->hasOne('App\Semester');
    }
}
