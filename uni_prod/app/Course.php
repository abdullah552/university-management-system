<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function books(){
        return $this->belongsToMany('App\Book', 'course_book');
        }
        
        
        public function departments(){
        return $this->belongsToMany('App\Department', 'course_department');
        }
    
}
