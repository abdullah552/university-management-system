<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function students(){
        return $this->belongsToMany('App\Student', 'student_department');
    }

    public function courses(){
        return $this->belongsToMany('App\Course', 'course_department');
    }
}
