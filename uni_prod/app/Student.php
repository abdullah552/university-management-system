<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function departments(){
        return $this->belongsToMany('App\Department', 'student_department');
    }

    public function prerequisite(){
        return $this->hasOne('App\Prerequisite');
    }

    public function faculties(){
        return $this->belongsToMany('App\Faculty');
    }
}
